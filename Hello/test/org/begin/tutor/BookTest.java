package org.begin.tutor;

import junit.framework.TestCase;

public class BookTest extends TestCase {
	public void testBook() {
		Book b1 = new Book("Great Expectation");
		assertEquals("Great Expectation", b1.title);
		assertEquals("unknown author", b1.author);
		
	}

	public void testGetPerson() {
		Book b2 = new Book("War and Peace");
		Person p2 = new Person();
		p2.setName("Evis");
		
		//method to say book is loaned by this person
		b2.setPerson(p2);
		
		//get the name of the person who loan the book
//		Person testPerson = b2.getPerson();
//		String testName = testPerson.getName();
		String testName = b2.getPerson().getName();
		
		assertEquals("Evis", testName);
		
	}
	
	public void testToStrings() {
		Book b2 = new Book("War and Peace");
		Person p2 = new Person();
		p2.setName("Evis");
		
		assertEquals("War and Peace by unknown author; Available", b2.toString());
		
		b2.setPerson(p2);
		assertEquals("War and Peace by unknown author; Checked out to Evis", b2.toString());
		
	}
}
