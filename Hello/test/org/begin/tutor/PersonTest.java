package org.begin.tutor;

import junit.framework.TestCase;

public class PersonTest extends TestCase {

	public void testPerson() {
		Person p1 = new Person();
		assertEquals("unknown name", p1.getName());
		assertEquals(3, p1.getMaxBook());
	}

	public void testSetName() {
		Person p2 = new Person();
		p2.setName("DuongPhamKhanh");
		assertEquals("DuongPhamKhanh", p2.getName());
	}

	public void testSetMaxBook() {
		Person p3 = new Person();
		p3.setMaxBook(10);
		assertEquals(10, p3.getMaxBook());
	}

	public void testToString() {
		Person p4 = new Person();
		p4.setName("Hoai Thu");
		p4.setMaxBook(5);
		String testString = "Hoai Thu (5 books)";
		assertEquals(testString, p4.toString());
		
	}
}
