package org.begin.tutor;

public class Book {

	String title;
	String author;
	Person person;

	public Book() {
		this.title = "Great Expectation";
		this.author = "Dicken";
	}
	
	public Book(String str) {
		this.title = str;
		this.author = "unknown author";
	}
	
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setPerson(Person p1) {
		this.person = p1;
		
	}

	public Person getPerson() {
		return this.person;
	}
	
	public String toString() {
		
		String available;
		if (this.getPerson() == null) {
			available = "Available";
		}
		else{
			available = "Checked out to " + this.getPerson().getName();
		}
		
		return this.getTitle() + " by " + this.getAuthor() + "; " + available;
		
	}

}
