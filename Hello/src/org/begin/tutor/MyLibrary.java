package org.begin.tutor;

import java.util.ArrayList;

public class MyLibrary {

	String name;
	ArrayList<Book> books;
	ArrayList<Person> people;

	public MyLibrary(String strName) {
		this.name = strName;
		books = new ArrayList<Book>();
		people = new ArrayList<Person>();
	}

	public String getName() {
		return name;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}

	public ArrayList<Person> getPeople() {
		return people;
	}

	public void addBook(Book b1) {
		this.books.add(b1);
	}

	public void removeBook(Book b1) {
		this.books.remove(b1);
	}

	public void addPerson(Person p1) {
		this.people.add(p1);
	}
	
	public void removePerson(Person p1) {
		this.people.remove(p1);
	}

	public boolean checkOut(Book b1, Person p1) {
		
		int bookOut = this.getBooksForPerson(p1).size();
		
		if ((b1.getPerson() == null) && 
				bookOut < p1.getMaxBook()) {
			b1.setPerson(p1);
			return true;
		}
		else {
			return false;
		}
	}

	public boolean checkIn(Book b1) {
		if (b1.getPerson() != null){
			b1.setPerson(null);
			return true;
		}
		else {
			return false;
		}
	}

	public ArrayList<Book> getBooksForPerson(Person p1) {
		
		ArrayList<Book> result = new ArrayList<Book>();
		for (Book aBook : this.getBooks()) {
			if ((aBook.getPerson() != null) && 
					(aBook.getPerson().getName().equals(p1.getName())))
			{
				result.add(aBook);
			}
			
		}
		
		return result;

	}

	public ArrayList<Book> getAvailableBooks() {
		
		ArrayList<Book> result = new ArrayList<Book>();
		
		for (Book aBook : this.getBooks()) {
			
			if (aBook.getPerson() == null){
				result.add(aBook);
			}
			
		}
		return result;
	}

	public ArrayList<Book> getUnavailableBooks(Person p1) {
		
		ArrayList<Book> result = new ArrayList<Book>();
		
		for (Book aBook : this.getBooks()) {
			
			if (aBook.getPerson() != null){
				result.add(aBook);
			}
			
		}
		return result;
	}
	
	public String toString() {
		return this.getName() + ": " + 
				this.getBooks().size() + " books; " +
				this.getPeople().size() + " people.";
	}
	
	public static void main(String[] args) {
		
		MyLibrary testLibrary = new MyLibrary("Test Drive Library");
		
		Book b1 = new Book("War and Peace");
		Book b2 = new Book("Great Expectation");
		b1.setAuthor("Tolstoy");
		b2.setAuthor("Dicken");
		
		Person jim = new Person();
		Person sue = new Person();
		jim.setName("Jim");
		sue.setName("Sue");
		
		testLibrary.addBook(b1);
		testLibrary.addBook(b2);
		testLibrary.addPerson(sue);
		testLibrary.addPerson(jim);
		
		System.out.println("Just create new Test Drive Library");
		testLibrary.printStatus();
		
		System.out.println("Check War and Peace to Sue");
		testLibrary.checkOut(b1, sue);
		testLibrary.printStatus();
		
		System.out.println("Do some more stuff");
		testLibrary.checkIn(b1);
		testLibrary.checkOut(b2, jim);
		testLibrary.printStatus();
		
	}

	private void printStatus() {
		
		System.out.println("Status Report of MyLibrary \n" + this.toString());
		
		for (Book aBook : this.getBooks()) {
			System.out.println(aBook);
		}
		
		for (Person p : this.getPeople()) {
			int count = this.getBooksForPerson(p).size();
			System.out.println(p + " has " + count + " of my books.");
		}
		
		System.out.println("Book available: " + this.getAvailableBooks().size());
		
		System.out.println("----- End of status report -----");
		
		
	}
	

}
