package org.begin.tutor;

public class Person {
	// fields
	private String name;	// name of person
	private int maxBook;	//most books person can check out
	
	//constructor
	public Person(){
		name="unknown name";
		maxBook = 3;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String anyName) {
		name = anyName;
	}
	
	public int getMaxBook() {
		return maxBook;
	}

	public void setMaxBook(int maxBook) {
		this.maxBook = maxBook;
	}

	public String toString() {
		return this.getName() + " (" + this.getMaxBook() + " books)";
		
	}
}
